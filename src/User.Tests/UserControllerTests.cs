﻿using Coscine.Api.User;
using Coscine.Api.User.Controllers;
using Coscine.Database.Models;
using Coscine.Database.ReturnObjects;
using Coscine.JwtHandler;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Routing;
using Moq;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System.Collections.Generic;
using System.IO;
using System.Security.Claims;

namespace User.Tests
{
    //[TestFixture]
    //public class UserControllerTests
    //{
    //    private readonly UserController _userController;
    //    private readonly ContactChangeController _contactChangeController;
    //    private readonly List<Coscine.Database.DataModel.User> _users = new List<Coscine.Database.DataModel.User>();

    //    public UserControllerTests()
    //    {
    //        _userController = new UserController();
    //        _contactChangeController = new ContactChangeController() { IsTest = true };
    //    }

    //    [OneTimeSetUp]
    //    public void Setup()
    //    {
    //        UserModel userModel = new UserModel();

    //        var user = new Coscine.Database.DataModel.User()
    //        {
    //            Givenname = "Test",
    //            Surname = "User",
    //            DisplayName = "Test User",
    //            EmailAddress = "testUser@test.com",
    //        };

    //        userModel.Insert(user);
    //        _users.Add(user);

    //        FakeControllerContext(user);
    //    }

    //    private void FakeControllerContext(Coscine.Database.DataModel.User user, Stream stream = null)
    //    {
    //        var request = new Mock<HttpRequest>();

    //        JWTHandler jwtHandler = new JWTHandler(Program.Configuration);
    //        Dictionary<string, string> values = new Dictionary<string, string>
    //        {
    //            { "userId", user.Id.ToString() }
    //        };
    //        request.SetupGet(x => x.Headers).Returns(
    //            new HeaderDictionary {
    //                {"X-Requested-With", "XMLHttpRequest"},
    //                {"Authorization", "Bearer " + jwtHandler.GenerateJwtToken(values)}
    //            }
    //        );

    //        var context = new Mock<HttpContext>();
    //        context.SetupGet(x => x.Request).Returns(request.Object);

    //        var claimsPrincipal = new Mock<ClaimsPrincipal>();
    //        Claim claim = new Claim("userId", user.Id.ToString());
    //        var list = new List<Claim>
    //        {
    //            claim
    //        };
    //        context.SetupGet(x => x.User).Returns(claimsPrincipal.Object);
    //        context.Setup(x => x.User.FindFirst("userId")).Returns(claim);
    //        context.SetupGet(x => x.User.Claims).Returns(list);

    //        if (stream != null)
    //        {
    //            context.SetupGet(x => x.Request.Method).Returns("POST");
    //            context.SetupGet(x => x.Request.HttpContext).Returns(context.Object);
    //            context.SetupGet(x => x.Request.Body).Returns(stream);
    //            context.SetupGet(x => x.Request.ContentLength).Returns(stream.Length);
    //            context.SetupGet(x => x.Request.ContentType).Returns("application/json");
    //        }

    //        var actionDescriptor = new Mock<ControllerActionDescriptor>();

    //        _userController.ControllerContext = new ControllerContext(new ActionContext(context.Object, new RouteData(), actionDescriptor.Object));
    //        _contactChangeController.ControllerContext = new ControllerContext(new ActionContext(context.Object, new RouteData(), actionDescriptor.Object));
    //    }

    //    [OneTimeTearDown]
    //    public void End()
    //    {
    //        UserModel userModel = new UserModel();
    //        foreach (var user in _users)
    //        {
    //            userModel.Delete(user);
    //        }
    //    }

    //    [Test]
    //    public void TestControllerChangeContactMail()
    //    {
    //        FakeControllerContext(_users[0]);

    //        string testEmailForChange = "email@changed.com";
    //        string testTokenForVerification;
    //        JObject email = new JObject() { ["Email"] = testEmailForChange };


    //        // TEST NEW CHANGE            
    //        var actionResultChange = _contactChangeController.ChangeContactEmail(email);
    //        Assert.IsTrue(actionResultChange.GetType() == typeof(OkObjectResult));
    //        var changeResult = (OkObjectResult)actionResultChange;
    //        ContactChangeObject contactChangeObject = (ContactChangeObject)changeResult.Value;
    //        Assert.IsTrue(contactChangeObject.NewEmail == testEmailForChange);
    //        testTokenForVerification = contactChangeObject.ConfirmationToken.ToString();


    //        // TEST STATUS
    //        var actionResultStatus = _contactChangeController.ConfirmationStatus();
    //        Assert.IsTrue(actionResultStatus.GetType() == typeof(OkObjectResult));
    //        var statusResult = (OkObjectResult)actionResultStatus;
    //        List<ContactChangeObject> contactChangeObjects = (List<ContactChangeObject>)statusResult.Value;
    //        Assert.IsTrue(contactChangeObjects.Count > 0);
    //        var emailList = new List<string>();
    //        foreach (var entry in contactChangeObjects)
    //        {
    //            emailList.Add(entry.NewEmail);
    //        }
    //        Assert.IsTrue(emailList.Contains(testEmailForChange));


    //        // TEST CONFIRMATION
    //        var actionResulConfirmation = _contactChangeController.ConfirmContactEmail(testTokenForVerification);
    //        Assert.IsTrue(actionResulConfirmation.GetType() == typeof(OkObjectResult));
    //        var confirmationResult = (OkObjectResult)actionResulConfirmation;
    //        UserObject userObject = (UserObject)confirmationResult.Value;
    //        Assert.IsTrue(userObject.EmailAddress == testEmailForChange);
    //    }

    //    [Test]
    //    public void TestControllerGetUser()
    //    {
    //        var user = _users[0];

    //        var actionResult = _userController.GetUser();
    //        Assert.IsTrue(actionResult.GetType() == typeof(OkObjectResult));
    //        OkObjectResult okObjectResult = (OkObjectResult)actionResult;
    //        UserObject userObject = (UserObject)okObjectResult.Value;

    //        Assert.IsTrue(userObject.Id == user.Id);
    //        Assert.IsTrue(userObject.DisplayName == user.DisplayName);
    //    }
    //}
}
