﻿using System.Collections.Generic;

namespace Coscine.Api.User.ReturnObjects
{
    /// <summary>
    /// Class representing the TOS version
    /// </summary>
    public class Tos
    {
        /// <summary>
        /// The TOS version
        /// </summary>
        public List<string> Version { get; set; } = new();
    }
}
