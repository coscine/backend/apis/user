﻿using Coscine.ApiCommons;
using Coscine.Configuration;

namespace Coscine.Api.User
{
    /// <summary>
    /// Standard Program class.
    /// </summary>
    public class Program : AbstractProgram<ConsulConfiguration>
    {
        /// <summary>
        /// Standard Main method.
        /// </summary>
        public static void Main()
        {
            InitializeWebService<Startup>();
        }
    }
}
