using Coscine.ApiCommons;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Coscine.ActiveDirectory;
using Coscine.Database.Models;
using Coscine.Database.ReturnObjects;
using Coscine.Database.Util;
using System.Collections.Generic;
using Coscine.JwtHandler;

namespace Coscine.Api.User.Controllers
{
    /// <summary>
    /// This controller represents the actions which can be taken with a user object.
    /// </summary>
    [Authorize]
    public class UserController : Controller
    {
        private readonly Authenticator _authenticator;
        private readonly UserModel _userModel;

        /// <summary>
        /// UserController constructor
        /// </summary>
        public UserController()
        {
            _authenticator = new Authenticator(this, Program.Configuration);
            _userModel = new UserModel();
        }

        /// <summary>
        /// Retrieves the UserObject.
        /// </summary>
        /// <returns>UserObject</returns>
        [HttpGet("[controller]/user")]
        public ActionResult<UserObject> GetUser()
        {
            var user = _authenticator.GetUser();
            return Ok(_userModel.CreateReturnObjectFromDatabaseObject(user));
        }

        /// <summary>
        /// Updates the User.
        /// </summary>
        /// <param name="userObject">Entry representing the user</param>
        /// <returns>StatusCode for Success</returns>
        [HttpPost("[controller]/user")]
        public ActionResult<int> UpdateUser([FromBody] UserObject userObject)
        {
            var user = _authenticator.GetUser();

            var displayName = userObject.Givenname + " " + userObject.Surname;
            if (userObject.Title != null && !string.IsNullOrWhiteSpace(userObject.Title.DisplayName))
            {
                displayName = userObject.Title.DisplayName + " " + displayName;
            }
            userObject.DisplayName = displayName.Trim();

            var result = _userModel.UpdateByObject(user, userObject);

            user.Givenname = userObject.Givenname;
            user.Surname = userObject.Surname;
            user.DisplayName = userObject.DisplayName;

            ADHandler.UpdateUser(user, Program.Configuration);

            return Ok(result);
        }

        /// <summary>
        /// This method lets someone query the user database using a query string.
        /// </summary>
        /// <param name="queryString">Search String</param>
        /// <param name="projectId">Project from which the search is triggered</param>
        /// <returns>The 10 top matched users are returned</returns>
        [HttpGet("[controller]/query/{queryString}/project/{projectId}")]
        public ActionResult<IEnumerable<UserObject>> Query(string queryString, string projectId)
        {
            var user = _authenticator.GetUser();
            string lowerQueryString = queryString.ToLower();
            Guid.TryParse(projectId, out Guid projectIdGuid);
            ProjectModel projectModel = new ProjectModel();
            ProjectRoleModel projectRoleModel = new ProjectRoleModel();

            // Deleted check in HasAccess
            if (projectModel.HasAccess(user, projectModel.GetById(projectIdGuid), UserRoles.Owner))
            {
                var projectRoles = projectRoleModel.GetAllWhere((projectRole) => projectRole.ProjectId == projectIdGuid);

                return Ok(_userModel.GetAllWhere((dbUser) =>
                    (dbUser.DisplayName.ToLower().Contains(lowerQueryString)
                    || dbUser.EmailAddress.ToLower().Equals(lowerQueryString)))
                        .Take(10)
                        .Select((dbUser) => new UserObject(dbUser.Id, dbUser.DisplayName, dbUser.Givenname, dbUser.Surname, dbUser.EmailAddress,
                                                           ((projectRoles.Any((projectRole) => projectRole.UserId == dbUser.Id)))) // return whether the user has any role in the project
                        ));
            }
            else
            {
                return Forbid("User is not allowed to query users with respect to given project!");
            }
        }

        /// <summary>
        /// Sets and returns a merge token.
        /// </summary>
        /// <param name="provider">Specifies the provider from which a user should be merged from</param>
        /// <returns>MergeToken as Json</returns>
        [HttpGet("[controller]/mergeToken/{provider}")]
        public ActionResult<string> SetAndReturnMergeToken(string provider)
        {
            provider = provider.ToLower();

            var providers = new[] { "orcid", "shibboleth" };
            if (!providers.Contains(provider))
            {
                return Forbid("The given provider is not registered!");
            }

            var user = _authenticator.GetUser();
            var externalAuthenticatorModel = new ExternalAuthenticatorModel();
            var connections = externalAuthenticatorModel.GetAllWhere((entry) =>
                (from externalId in entry.ExternalIds
                 where externalId.UserId == user.Id
                 select externalId).Count() > 0
                 && entry.DisplayName.ToLower() == provider);
            if (connections.Count() > 0)
            {
                return Forbid("The given provider is already connected to the user!");
            }

            var jwtHandler = new JWTHandler(Program.Configuration);
            var jwt = jwtHandler.GenerateJwtToken(new Dictionary<string, string>
            {
                { "LoginMethod", provider },
                { "UserGuid", user.Id.ToString() }
            });

            var mergeTokenKey = "coscine.mergetoken";
            if (Request.Cookies.ContainsKey(mergeTokenKey))
            {
                Response.Cookies.Delete(mergeTokenKey);
            }
            Response.Cookies.Append(mergeTokenKey, jwt);

            return Json(jwt);
        }
    }
}
