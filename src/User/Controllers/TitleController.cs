using Coscine.Database.Models;
using Coscine.Database.ReturnObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.Api.User.Controllers
{
    /// <summary>
    /// This controller represents the actions which can be taken with a title object.
    /// </summary>
    [Authorize]
    public class TitleController : Controller
    {
        private readonly TitleModel _titleModel;

        /// <summary>
        /// TitleController constructor specifying a TitleModel.
        /// </summary>
        public TitleController()
        {
            _titleModel = new TitleModel();
        }

        /// <summary>
        /// Returns all available titles.
        /// </summary>
        /// <returns>All Titles</returns>
        [Route("[controller]")]
        public ActionResult<IEnumerable<TitleObject>> Index()
        {
            return Json(_titleModel.GetAll()
                .Select((title) => new TitleObject(title.Id, title.DisplayName)));
        }
    }
}
