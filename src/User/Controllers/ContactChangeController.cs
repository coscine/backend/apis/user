﻿using Coscine.Action;
using Coscine.Action.EventArgs;
using Coscine.ApiCommons;
using Coscine.Configuration;
using Coscine.Database.Models;
using Coscine.Database.ReturnObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Coscine.Api.User.Controllers
{
    /// <summary>
    /// This controller represents the actions which can be taken with a ContactChange object.
    /// </summary>
    [Authorize]
    public class ContactChangeController : Controller
    {
        private readonly Authenticator _authenticator;
        private readonly ContactChangeModel _contactChangeModel;
        private readonly IConfiguration _configuration;
        private readonly Emitter _emitter;

        /// <summary>
        /// ContactChangeController constructor
        /// </summary>
        public ContactChangeController()
        {
            _authenticator = new Authenticator(this, Program.Configuration);
            _contactChangeModel = new ContactChangeModel();
            _configuration = Program.Configuration;
            _emitter = new Emitter(_configuration);
        }

        /// <summary>
        /// This method enables the user to request a change of his or her contact email. After entering a valid email, a confirmation email will be sent to the new address and a notification email will be sent to the present address.
        /// </summary>
        /// <param name="emailAddress">A new email as string set by the user.</param>
        /// <returns>Output ContactChangeObject. </returns>
        /// <response code="200">If no pending emails exist. Emails will be sent out. </response>
        /// <response code="202">If there is already a pending email change. In this case the old entry or entries will be deleted, which will directly invalidate all previous tokens. </response>
        /// <response code="422">If the email is null or not a valid email format. </response>
        [HttpPost("[controller]/emailchange")]
        public ActionResult<ContactChangeObject> ChangeContactEmail([FromBody] string emailAddress)
        {
            var user = _authenticator.GetUser();
            if (emailAddress != null && new EmailAddressAttribute().IsValid(emailAddress))
            {
                if (_contactChangeModel.UserHasEmailsToConfirm(user.Id))
                {
                    ContactChangeObject contactChangeObject = _contactChangeModel.NewEmailChangeRequest(user.Id, emailAddress);
                    ContactChangeEventArgs contactChangeEventArgs = new ContactChangeEventArgs(_configuration)
                    {
                        ContactInformation = contactChangeObject
                    };
                    _emitter.EmitContactChange(contactChangeEventArgs);
                    return Accepted(contactChangeObject); // Return Code: 202 - Accepted
                }
                else
                {
                    ContactChangeObject contactChangeObject = _contactChangeModel.NewEmailChangeRequest(user.Id, emailAddress);
                    ContactChangeEventArgs contactChangeEventArgs = new ContactChangeEventArgs(_configuration)
                    {
                        ContactInformation = contactChangeObject
                    };
                    _emitter.EmitContactChange(contactChangeEventArgs);
                    return Ok(contactChangeObject); // Return Code: 200 - OK
                }
            }
            else
            {
                return UnprocessableEntity("Incorrect E-Mail format!"); // Return Code: 422 - Unprocessable Entity
            }
        }

        /// <summary>
        /// Calling this method checks whether or not the entered confirmation token (verification) exists inside the database for the current user. If yes, it updates the EmailAddress field for that user with
        /// the New Email that has been requested with the ChangeContactEmail method.
        /// </summary>
        /// <param name="verification">The Confirmation Token as String for a New Email.</param>
        /// <returns>Output UserObject. </returns>
        /// <response code="200">If email confirmation is successful. Output UserObject as JSON. </response>
        /// <response code="400">If Token has expired, or its EditDate is NULL. </response>
        /// <response code="404">If Token and User ID combinations invalid. </response>
        [AllowAnonymous]
        [HttpGet("[controller]/confirm/{verification}")]
        public ActionResult<UserObject> ConfirmContactEmail(string verification)
        {
            try
            {
                Guid token = Guid.Parse(verification);
                UserObject userObject = _contactChangeModel.ExecuteConfirmation(token);
                return Ok(userObject); // Return Code: 200 - OK
            }
            catch (ArgumentNullException e)
            {
                // INVALID (null EditDate)
                return BadRequest(e.Message); // Return Code: 400 - Bad Request
            }
            catch (MissingFieldException e)
            {
                // INVALID (token-user combination not in Database)
                return NotFound(e.Message); // Return Code: 404 - Not Found
            }
            catch (Exception e)
            {
                // TOKEN EXPIRED
                return BadRequest(e.Message); // Return Code: 400 - Bad Request
            }
        }

        /// <summary>
        /// Calling this method will check if there are pending emails for confirmation for the current user.
        /// </summary>
        /// <returns>Output List of ContactChangeObjects. </returns>
        /// <response code="200">If there is already a pending email change. </response>
        /// <response code="204">If there is no pending email change (database empty). </response>
        [HttpGet("[controller]/status")]
        public ActionResult<IEnumerable<ContactChangeObject>> ConfirmationStatus()
        {
            var user = _authenticator.GetUser();
            if (_contactChangeModel.UserHasEmailsToConfirm(user.Id))
            {
                return Ok(_contactChangeModel.GetEmailsForConfirmation(user.Id)); // Return Code: 200 - OK
            }
            else
            {
                return NoContent(); // Return Code: 204 - No Content
            }
        }
    }
}