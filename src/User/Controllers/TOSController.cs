using Coscine.ApiCommons;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Coscine.Database.Models;
using Coscine.Database.DataModel;
using Newtonsoft.Json.Linq;
using Coscine.Api.User.ReturnObjects;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.Api.User.Controllers
{
    /// <summary>
    /// This controller represents the actions which can be taken with a tos object.
    /// </summary>
    [Authorize]
    public class TOSController : Controller
    {
        private readonly Authenticator _authenticator;
        private readonly TOSModel _tosModel;

        /// <summary>
        /// TOSController constructor specifying an authenticator and a TOSModel.
        /// </summary>
        public TOSController()
        {
            _authenticator = new Authenticator(this, Program.Configuration);
            _tosModel = new TOSModel();
        }

        /// <summary>
        /// Returns the current TOS Version.
        /// </summary>
        /// <returns>Current TOS Version as Json</returns>
        [AllowAnonymous]
        [HttpGet("[controller]/version")]
        public ActionResult<string> GetCurrentTOSVersion()
        {
            return Ok(Program.Configuration.GetStringAndWait("coscine/global/tos/version"));
        }

        /// <summary>
        /// Returns the TOS Versions accepted by the current user.
        /// </summary>
        /// <returns>StatusCode</returns>
        [HttpGet("[controller]")]
        public ActionResult<Tos> AcceptedTOSVersion()
        {
            var user = _authenticator.GetUser();
            var tosAccepted = _tosModel.GetAllWhere(tos => tos.UserId.Equals(user.Id));
            
            if (tosAccepted is not null)
            {
                return Ok(new Tos
                {
                    Version = tosAccepted.Select(tos => tos.Version).Distinct().ToList()
                });;
            }
            else
            {
                return NotFound();
            }
        }

        /// <summary>
        /// Calling this method accepts the TOS for the current user.
        /// </summary>
        /// <returns>StatusCode</returns>
        [HttpPost("[controller]")]
        public ActionResult<string> AcceptCurrentTOSVersion()
        {
            var user = _authenticator.GetUser();
            var tosAccepted = new Tosaccepted
            {
                UserId = user.Id,
                Version = Program.Configuration.GetStringAndWait("coscine/global/tos/version")
            };

            var isAlreadyAccepted = _tosModel.GetAllWhere(t => t.UserId.Equals(user.Id) && t.Version.Equals(tosAccepted.Version)).Any();
            if (isAlreadyAccepted)
            {
                return BadRequest($"TOS Version \"{tosAccepted.Version}\" is already accepted by the user.");
            }
            _tosModel.Insert(tosAccepted);
            return Ok(tosAccepted.Version);
        }
    }
}
