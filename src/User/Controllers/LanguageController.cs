using Coscine.Database.Models;
using Coscine.Database.ReturnObjects;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;

namespace Coscine.Api.User.Controllers
{
    /// <summary>
    /// This controller represents the actions which can be taken with a language object.
    /// </summary>
    [Authorize]
    public class LanguageController : Controller
    {
        private readonly LanguageModel _languageModel;

        /// <summary>
        /// LanguageController constructor specifying a LanguageModel.
        /// </summary>
        public LanguageController()
        {
            _languageModel = new LanguageModel();
        }

        /// <summary>
        /// Returns all available languages.
        /// </summary>
        /// <returns>Languages</returns>
        [Route("[controller]")]
        public ActionResult<IEnumerable<LanguageObject>> Index()
        {
            return Json(_languageModel.GetAll()
                .Select((language) => new LanguageObject(language.Id, language.DisplayName, language.Abbreviation)));
        }
    }
}
